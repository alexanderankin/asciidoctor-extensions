package com.puravida.asciidoctor;
import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class HttpsTest {

    @Test
    public void postProcessorIsApplied() {

        Map<String,Object> attributes = new HashMap<String,Object>();
        attributes.put("google-analytics-code","UA-aaaaa-aa");

        Options options = new Options();
        options.setHeaderFooter(true);
        options.setSafe(SafeMode.SERVER);

        options.setAttributes(attributes);
        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        String converted = asciidoctor.convert(String.format(
                "= My document\n" +
                        ":toc: left\n" +
                        ":toc-collapsable: \n" +
                        ":ensure-https: \n" +
                        "== Hola \n"+
                        "caracola \n"+
                        "== Hello \n"+
                        "caracol \n"+
                        "=== Hi \n"+
                        "caracol \n"+
                        ""), options);
        assertThat(converted).contains("location.protocol = 'https:';");
    }


}
