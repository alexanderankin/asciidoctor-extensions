package com.puravida.asciidoctor;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class ForEachTests {

    @Test
    public void preProcessorIsApplied() {
        Map<String,Object> attributes = new HashMap<String,Object>();

        Options options = new Options();
        options.setHeaderFooter(true);
        options.setSafe(SafeMode.SERVER);

        options.setAttributes(attributes);
        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        String converted = asciidoctor.convert(String.format(
                "= My document\n\n" +
                        "preambulo \n"+
                        "[each-file,pattern=(.)*.java,dir=src/main/java/com/puravida/asciidoctor,sort=name,order=asc] \n"+
                        "++++\n"+
                        "Hola {filename}\n\n"+
                        "\\include::src/main/java/com/puravida/asciidoctor/{filename}[lines=1..3]\n\n"+
                        "++++\n"+
                        "footer \n"+
                        ""), options);
        System.out.println(converted);
        assertThat(converted).contains("Hola ReadResources.java");
        assertThat(converted).contains("Hola AsciidoctorExtensionRegistry.java");
        assert converted.indexOf("Hola AsciidoctorExtensionRegistry.java") < converted.indexOf("Hola ReadResources.java");
    }


}
