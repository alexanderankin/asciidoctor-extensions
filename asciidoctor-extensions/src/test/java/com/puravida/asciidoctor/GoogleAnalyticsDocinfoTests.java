package com.puravida.asciidoctor;
import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by jorge on 10/06/17.
 */
public class GoogleAnalyticsDocinfoTests {
    @Test
    public void postProcessorIsApplied() {
        Map<String,Object> attributes = new HashMap<String,Object>();
        attributes.put("google-analytics-code","UA-aaaaa-aa");

        Options options = new Options();
        options.setHeaderFooter(true);
        options.setSafe(SafeMode.SERVER);

        options.setAttributes(attributes);
        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        String converted = asciidoctor.convert(String.format(
                "= My document\n" +
                ":google-analytics-code: UA-000000-00\n"+
                "Hola caracola"), options);
        assertThat(converted).contains("ga('create', 'UA-aaaaa-aa', 'auto');");
    }

}
