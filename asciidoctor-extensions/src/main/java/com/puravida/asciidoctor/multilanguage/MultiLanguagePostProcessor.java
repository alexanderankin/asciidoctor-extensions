package com.puravida.asciidoctor.multilanguage;

import com.puravida.asciidoctor.ReadResources;
import org.asciidoctor.ast.Document;
import org.asciidoctor.extension.Postprocessor;

import java.io.File;


public class MultiLanguagePostProcessor extends Postprocessor {

    public static final String TAG = "multilanguage";

    public static final String TAG_TOOLBAR = "multilanguage-toolbar";

    public MultiLanguagePostProcessor(){

    }

    @Override
    public String process(Document document, String output) {

        String code = (String)(document.getAttributes().get(TAG));
        if( code == null)
            return output;

        String position = (String)(document.getAttributes().get(TAG_TOOLBAR));

        String[]languages = code.split(",");
        if(languages.length==0){
            return output;
        }

        String defaultLang = languages[0];

        StringBuffer stringBuffer = new StringBuffer();
        for(int i=0; i<languages.length; i++){
            stringBuffer.append("'"+languages[i]+"',");
        }
        stringBuffer.deleteCharAt(stringBuffer.length()-1);

        Object to_dir = document.getOptions().get("to_dir");
        if( to_dir != null ) {
            ReadResources.extractResourceToFile("/multilanguage/flags/blank.gif", new File(to_dir+"/blank.gif"));
            ReadResources.extractResourceToFile("/multilanguage/flags/flags.png", new File(to_dir+"/flags.png"));
        }
        output = ReadResources.addCss(output, "/multilanguage/flags/flags.css");
        output = ReadResources.includeJQuery(output);
        output = ReadResources.addJs(output, "/multilanguage/multilanguage.js")
                .replace("var defaultLang='CHANGEME';","var defaultLang='"+defaultLang+"';")
                .replace("var languages = ['LANG1'];","var languages = ["+stringBuffer.toString()+"];");

        if( position != null)
                output = output.replace("var toolbar='CHANGEME';",
                        "var toolbar='"+MultiLanguageMacroProcessor.buildToolbar(code)+"';")
                        .replace("var position='none';",
                                "var position='"+position+"';");
        return output;
    }

}
