package com.puravida.asciidoctor.disqus;
import com.puravida.asciidoctor.ReadResources;
import org.asciidoctor.ast.Block;
import org.asciidoctor.ast.Document;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.BlockMacroProcessor;
import org.asciidoctor.extension.Postprocessor;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Map;

/**
 * Created by jorge on 11/06/17.
 */
public class DisqusMacroProcessor extends BlockMacroProcessor{

    public static final String TAG = "disqus";

    public DisqusMacroProcessor(String macroName, Map<String, Object> config) {
        super(macroName, config);
    }

    @Override
    public Block process(StructuralNode parent, String target, Map<String, Object> attributes) {

        String disqus = ReadResources.readResource("/disqus.html");
        disqus=disqus.replace("{DISQUS-PROJECT-HERE}", target);

        String javascript = "<script id=\"dsq-count-scr\" src=\"//"+target+".disqus.com/count.js\" async></script>";

        String content = disqus+javascript;

        return createBlock(parent, "pass", Arrays.asList(content), attributes);
    }


}
