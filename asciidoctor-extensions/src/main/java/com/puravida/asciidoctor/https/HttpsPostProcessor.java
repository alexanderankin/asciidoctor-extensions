package com.puravida.asciidoctor.https;

import com.puravida.asciidoctor.ReadResources;
import org.asciidoctor.ast.Document;
import org.asciidoctor.extension.Postprocessor;

/**
 * Created by jorge on 17/05/18.
 */
public class HttpsPostProcessor extends Postprocessor {

    public static final String TAG = "ensure-https";

    public HttpsPostProcessor(){

    }

    @Override
    public String process(Document document, String output) {

        String code = (String)(document.getAttributes().get(TAG));
        if( code == null)
            return output;

        output = ReadResources.includeJQuery(output);
        output = ReadResources.addJs(output,"/https.js");

        return output;
    }

}
