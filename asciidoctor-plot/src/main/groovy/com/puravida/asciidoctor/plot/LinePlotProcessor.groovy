package com.puravida.asciidoctor.plot

import com.puravida.plot.PlotBuilder

class LinePlotProcessor extends AbstractPlotProcessor{

    LinePlotProcessor(String name, Map<String, Object> config) {
        super(name,config)
    }

    ByteArrayOutputStream executeClosure(Closure closure){
        PlotBuilder.instance.linePlot closure
    }

}
