package com.puravida.asciidoctor.plot

import groovy.util.logging.Log
import org.asciidoctor.ast.StructuralNode
import org.asciidoctor.extension.BlockProcessor
import org.asciidoctor.extension.Reader

import java.util.concurrent.TimeUnit


@Log
class GnuPlotProcessor extends BlockProcessor{

    GnuPlotProcessor(String name, Map<String, Object> config) {
        super(name, [contexts: [':literal'], content_model: ':simple'])
    }

    def process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {
        String rootDir = parent.document.options.find{"$it.key"=='to_dir'}.value ?: '.'
        File fRootDir = new File(rootDir)
        if( !fRootDir.isDirectory() )
            fRootDir=fRootDir.parentFile

        String imagedir = parent.document.attributes['imagesdir'] ?: ''
        File fImageDir = new File("$fRootDir.absolutePath/$imagedir")
        fImageDir.mkdirs()

        String filename = attributes.get(2L) ?: "${new Date().time}.png"
        filename=parent.normalizeWebPath(filename,"",true)

        int width = (attributes.get(3L) ?: "400") as int
        int height = (attributes.get(4L) ?: "400") as int

        plotFunction(reader,"$fImageDir.absolutePath/$filename",width,height)

        Map<String,Object> attr = [:]
        attr.target = filename
        attr.alt = "$filename".toString()
        attr.width =  "$width".toString()
        attr.height =  "$height".toString()

        Map<Object,Object>options= [:]

        createBlock(parent,"image","", attr,options)
    }

    void plotFunction(Reader reader, String filename, int w, int h){
        try {
            String commandLine = "gnuplot -e \"set terminal png size $w,$h; set output '${filename}';  "
            commandLine += reader.lines().join(';') + " \" "
            commandLine.execute().waitFor(10, TimeUnit.SECONDS)
        }catch(e){
            e.printStackTrace()
            log.severe "$filename: ${e.toString()}"
        }
    }
}
