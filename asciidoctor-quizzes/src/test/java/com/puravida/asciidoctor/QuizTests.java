package com.puravida.asciidoctor;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class QuizTests {

    @Test
    public void postProcessorIsApplied() {

        Options options = new Options();
        options.setHeaderFooter(true);
        options.setSafe(SafeMode.SERVER);

        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        String converted = asciidoctor.convert(String.format(
                "= My document\n" +
                        ":toc: left\n" +
                        ":toc-collapsable: \n" +
                        "== Hola \n"+
                        "caracola \n"+
                        "====\n"+
                        "this is the *text* of the quiz\n\n"+
                        "video::123123[youtube]\n\n"+
                        "quizquestion:1[10]\n\n"+
                        "quizquestion:1[0]\n\n"+
                        "====\n\n"+
                        "[#bloque2,source,groovy]\n"+
                        "----\n"+
                        "other text\n"+
                        "----\n"+
                        "[source]\n"+
                        "----\n"+
                        "this is not a id block\n"+
                        "----\n"+
                        ""), options);
        System.out.println(converted);
        //assertThat(converted).contains("data-clipboard-target=\"#bloque1\"");
    }

}
