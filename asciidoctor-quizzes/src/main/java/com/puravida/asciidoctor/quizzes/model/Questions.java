package com.puravida.asciidoctor.quizzes.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class Questions {

    private String id;

    private List<Question> questions = new ArrayList<Question>();

    public Questions(String id){
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    private static Pattern p = Pattern.compile( "([0-9]*)" );


    public Question addQuestion(Map<String,Object> attributes){
        Question question = new Question();
        question.setId(id+"-"+questions.size());
        question.setName(id);
        for(Map.Entry<String,Object>entry : attributes.entrySet()) {
            switch(entry.getKey()){
                case "1":
                    String points = entry.getValue().toString();
                    if( p.matcher(points).matches() )
                        question.setPoints(Integer.parseInt(points));
                    break;
                case "2":
                    question.setId(id+"-"+entry.getValue().toString());
                    break;
            }
        }
        questions.add(question);
        return question;
    }

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();

        stringBuffer.append("\"").append(id).append("\" : {");
        for(Question q : questions){
            stringBuffer.append(q).append(",");
        }
        stringBuffer.deleteCharAt(stringBuffer.length()-1);
        stringBuffer.append("}");

        return stringBuffer.toString();
    }
}
