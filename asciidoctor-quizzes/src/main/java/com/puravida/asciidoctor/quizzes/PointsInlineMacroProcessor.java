package com.puravida.asciidoctor.quizzes;

import com.puravida.asciidoctor.quizzes.model.Questions;
import com.puravida.asciidoctor.quizzes.model.Quiz;
import org.asciidoctor.ast.ContentNode;
import org.asciidoctor.extension.Format;
import org.asciidoctor.extension.FormatType;
import org.asciidoctor.extension.InlineMacroProcessor;
import org.asciidoctor.extension.Name;

import java.util.Map;

@Name("quizzpoints")
@Format(value=FormatType.LONG,regexp = "quizzpoints:([A-Za-z0-9]+)\\[(.*?)\\]")
public class PointsInlineMacroProcessor extends InlineMacroProcessor {

    @Override
    public Object process(ContentNode parent, String target, Map<String, Object> attributes) {
        Quiz quiz = (Quiz)parent.getDocument().getAttributes().get(QuizzPreprocessor.TAG);
        if (quiz == null){
            return null;
        }
        if( quiz.getQuestions().containsKey(target) == false )
            return null;
        Questions questions = quiz.getQuestions().get(target);
        return html(target,questions, attributes);
    }

    String html(String target, Questions questions, Map<String, Object> attributes){
        return String.format("<span id='%s'>%s</span>", "points-"+questions.getId(),"");
    }
}
