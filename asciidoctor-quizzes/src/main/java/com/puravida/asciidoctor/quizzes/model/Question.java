package com.puravida.asciidoctor.quizzes.model;

public class Question {

    private String id;

    private String name;

    private int points;

    private String type = "radio";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getPoints() {
        return points;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        StringBuffer ret = new StringBuffer();
        ret.append("\"").append(id).append("\" : ").append(points);
        return ret.toString();
    }

}
